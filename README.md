### Nike Interview Frontend Code Challenge [Vue]

_The provided code document should contain more details._

## Prerequisite

OS: `MacOS Monterey Version 12.2.2`

1. Install `NVM`: https://github.com/nvm-sh/nvm
2. Install Nginx: https://medium.com/@ThomasTan/installing-nginx-in-mac-os-x-maverick-with-homebrew-d8867b7e8a5a, which is required by gitlab-ci.
   ```
    Docroot is: /usr/local/var/www

    The default port has been set in /usr/local/etc/nginx/nginx.conf to 8080 so that
    nginx can run without sudo.

    nginx will load all files in /usr/local/etc/nginx/servers/.

    To restart nginx after an upgrade:
    brew services restart nginx
    Or, if you don't want/need a background service you can just run:
    /usr/local/opt/nginx/bin/nginx -g daemon off;

    Check nginx log by default: /usr/local/var/log/nginx/

   ```
After installing nginx, can replace default config with the `nginx.conf` in project directory.
3. Run `nvm install` to setup node version
4. For gitlab-ci, the shell executor is using bash, make sure to switch to `bash`.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development (Port 8080)
```
npm run serve
```

### Lints and fixes files
```
npm run lint
```

You need to start backend server (on port 8081), to be able to see live data in the frontend app.

### Showcase

[<img src="https://gitlab.com/jeaimesoleil/julian-ns-web-app/-/raw/main/showcase.png">](https://gitlab.com/jeaimesoleil/julian-ns-web-app/-/raw/main/showcase.png)

### Retrospect

#### Description of your changes & code improvements
1. Load shoeDatabase from API `/shoe/database`.
2. Add NIKI favicon.
3. Add a table column - DiscountPrice.
4. Refresh table at every 5s.

#### If you had more time, what would you add further
1. Booking the shoe and increase/decrease the inventory

#### What were your doubts, and what were your assumptions for the projects
N/A

#### Any other notes, that are relevant to your task
1. Trying to add CI/CD with runner. There are shared and local runners. No idea which one I should use to align with the interviewer's environment.
